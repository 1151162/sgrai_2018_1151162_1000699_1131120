#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DEBUG 1
#define LADO_MAXIMO     2
#define LADO_MINIMO     0.3
#define DELTA_LADO      0.1


/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	GLboolean   debug;
	GLboolean    movimentoTranslacao;      // se os cubinhos se movem
	GLboolean   movimentoRotacao;         // se o cubo grande roda;
}Estado;

typedef struct {
	GLint     altura;
	GLint     largura;
	GLint     profundidade;
	GLfloat   color1;
	GLfloat   color2;
	GLfloat   color3;
}Armario;


typedef struct {
	GLfloat   theta[3];     // 0-Rota��o em X; 1-Rota��o em Y; 2-Rota��o em Z

	GLint     eixoRodar;    // eixo que est� a rodar (mudar com o rato)
	GLfloat   ladoCubo;     // comprimento do lado do cubo
	GLfloat   deltaRotacao; // incremento a fazer ao angulo quando roda
	GLboolean  sentidoTranslacao; //sentido da transla��o dos cubos pequenos
	GLfloat    translacaoCubo; //
	GLfloat   deltaTranslacao; // incremento a fazer na translacao
	GLboolean sentidoRotacao;  //sentido da rota��o dos cubos pequenos
	GLfloat   thetaCubo;     // Rota��o dos cubinhos
}Modelo;

Estado estado;
Modelo modelo;
Armario armario;


/* Inicializa��o do ambiente OPENGL */
void inicia_modelo()
{
	estado.delayMovimento = 50;
	estado.movimentoTranslacao = GL_FALSE;
	estado.movimentoRotacao = GL_FALSE;

	modelo.theta[0] = 0;
	modelo.theta[1] = 0;
	modelo.theta[2] = 0;
	modelo.eixoRodar = 0;  // eixo de X;
	modelo.ladoCubo = 1;
	/* Aula 5*/
	modelo.deltaRotacao = 5;
	modelo.deltaTranslacao = 1;
	modelo.sentidoTranslacao = GL_TRUE;
	modelo.translacaoCubo = LADO_MAXIMO * 2.0;
	modelo.thetaCubo = 0.0;
}

void Init(void)
{
	inicia_modelo();
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	//glEnable(GL_DEPTH_TEST);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

/* CALLBACK PARA REDIMENSIONAR JANELA */
void Reshape(int width, int height)
{
	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width < height)
		glOrtho(-5, 5, -5 * (GLdouble)height / width, 5 * (GLdouble)height / width, -10, 10);
	else
		glOrtho(-5 * (GLdouble)width / height, 5 * (GLdouble)width / height, -5, 5, -10, 10);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


/* ... definicao das rotinas auxiliares de desenho ... */

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat cor[])
{

	/* draw a polygon via list of vertices */

	glBegin(GL_POLYGON);
	glColor3fv(cor);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}

/*void eixo() {

	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(2.0, 0.0, 0.0);
	//glTranslatef(-2.0, 0.0, 0.0);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.0, 2.0, 0.0);
	//glTranslatef(-2.0, 0.0, 0.0);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.0, 0.0, 0.0);
	glVertex3f(0.0, 0.0, 2.0);
	//glTranslatef(-2.0, 0.0, 0.0);
	glEnd();



}*/


void desenhaArmario(Armario armario)
{
	int largura = armario.largura;
	int altura = armario.altura;
	int profundidade = armario.profundidade;

	GLfloat vertice0[] = { 0 - largura/2, 0 - altura/2, 0 - profundidade/2 };
	GLfloat vertice1[] = { largura/2, 0 - altura/2, 0 - profundidade/2 };
	GLfloat vertice2[] = { largura/2, altura / 2, -profundidade / 2 };
	GLfloat vertice3[] = { -largura / 2, altura / 2, -profundidade / 2 };
	GLfloat vertice4[] = { -largura / 2, -altura / 2, profundidade / 2 };
	GLfloat vertice5[] = { largura / 2, -altura / 2, profundidade / 2 };
	GLfloat vertice6[] = { largura / 2, altura / 2, profundidade / 2 };
	GLfloat vertice7[] = { -largura / 2, altura / 2, profundidade / 2 };


	/*GLfloat vertice0[] = { -0.5,-0.5,-0.5 };
	GLfloat vertice1[] = { 0.5,-0.5,-0.5 };
	GLfloat vertice2[] = { 0.5,0.5,-0.5 };
	GLfloat vertice3[] = { -0.5,0.5,-0.5 };
	GLfloat vertice4[] = { -0.5,-0.5,0.5 };
	GLfloat vertice5[] = { 0.5,-0.5,0.5 };
	GLfloat vertice6[] = { 0.5,0.5,0.5 };
	GLfloat vertice7[] = { -0.5,0.5,0.5 };*/

	/*GLfloat vertices[][3] = { {-0.5,-0.5,-0.5},
							 {0.5,-0.5,-0.5},
							 {0.5,0.5,-0.5},
							 {-0.5,0.5,-0.5} };

	GLfloat vertices2[][3] = { {-0.5,-0.5,0.5},
							 {0.5,-0.5,0.5},
							 {0.5,0.5,0.5},
							 {-0.5,0.5,0.5} };*/

	GLfloat cor[3] = { armario.color1,armario.color2,armario.color3 };


	//desenhaPoligono(vertice4, vertice7, vertice6, vertice5, cores[0]);
	desenhaPoligono(vertice5, vertice6, vertice2, vertice1, cor);
	desenhaPoligono(vertice0, vertice1, vertice2, vertice3, cor);
	desenhaPoligono(vertice0, vertice3, vertice7, vertice4, cor);
	desenhaPoligono(vertice2, vertice3, vertice7, vertice6, cor);
	desenhaPoligono(vertice0, vertice1, vertice5, vertice4, cor);
	/*desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], cores[0]);
	desenhaPoligono(vertices2[1], vertices2[0], vertices2[3], vertices2[2], cores[1]);*/
}


/* Callback de desenho */
void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/* ... chamada das rotinas auxiliares de desenho ... */

	glPushMatrix();
    glRotatef(modelo.theta[0], 1, 0, 0);
    glRotatef(modelo.theta[1], 0, 1, 0);
    glRotatef(modelo.theta[2], 0, 0, 1);


	glScalef(modelo.ladoCubo, modelo.ladoCubo, modelo.ladoCubo);
	int altura = 20;
	int largura = 8;
	int profundidade = 5;
	float color1 = 0.0;
	float color2 = 1.0;
	float color3 = 1.0;
	armario.color1 = color1;
	armario.color2 = color2;
	armario.color3 = color3;
	armario.altura = altura;
	armario.largura = largura;
	armario.profundidade = profundidade;
	desenhaArmario(armario);
	//eixo();


	glPushMatrix();
	glTranslatef(-2.0, 0.0, 0.0);
	glScalef(0.3, 0.3, 0.3);
	//eixo();
	glPopMatrix();

	if (estado.doubleBuffer)
		glutSwapBuffers();
	else
		glFlush();

}

/*******************************
***   callbacks timer   ***
*******************************/
/* Callback de temporizador */
void Timer(int value)
{

	glutTimerFunc(estado.delayMovimento, Timer, 0);
	if (estado.movimentoRotacao) {
		modelo.theta[modelo.eixoRodar] += modelo.deltaRotacao;
		if (modelo.theta[modelo.eixoRodar] >= 360) modelo.theta[modelo.eixoRodar] -= 360;
	}
	/* ... accoes do temporizador n�o colocar aqui transforma��es, alterar
	   somente os valores das vari�veis ... */

	   // alterar o modelo.theta[] usando a vari�vel modelo.eixoRodar como indice


	   /* redesenhar o ecr� */
	glutPostRedisplay();
}


/*******************************
***  callbacks de teclado    ***
*******************************/

void imprime_ajuda(void)
{
	printf("\n\nDesenho de um quadrado\n");
	printf("h,H - Ajuda \n");
	printf("F1  - Reiniciar \n");
	printf("F2  - Poligono Fill \n");
	printf("F3  - Poligono Line \n");
	printf("F4  - Poligono Point \n");
	printf("+   - Aumentar tamanho dos Cubos\n");
	printf("-   - Diminuir tamanho dos Cubos\n");
	printf("i,I - Reiniciar Vari�veis\n");
	printf("p,p - Iniciar/Parar movimento dos cubinhos\n");
	printf("ESC - Sair\n");
	printf("teclas do rato para iniciar/parar rota��o e alternar eixos\n");

}


/* Callback para interaccao via teclado (carregar na tecla) */
void Key(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(1);
		/* ... accoes sobre outras teclas ... */

	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case '+':
		if (modelo.ladoCubo < LADO_MAXIMO)
		{
			modelo.ladoCubo += DELTA_LADO;
			glutPostRedisplay();
		}
		break;

	case '-':
		if (modelo.ladoCubo > LADO_MINIMO)
		{
			modelo.ladoCubo -= DELTA_LADO;
			glutPostRedisplay();
		}
		break;

	case 'i':
	case 'I':
		inicia_modelo();
		glutPostRedisplay();
		break;
	case 'p':
	case 'P':
		estado.movimentoTranslacao = !estado.movimentoTranslacao;
		break;

	}

	if (DEBUG)
		printf("Carregou na tecla %c\n", key);

}

/* Callback para interaccao via teclado (largar a tecla) */
void KeyUp(unsigned char key, int x, int y)
{

	if (DEBUG)
		printf("Largou a tecla %c\n", key);
}

/* Callback para interaccao via teclas especiais  (carregar na tecla) */
void SpecialKey(int key, int x, int y)
{
	/* ... accoes sobre outras teclas especiais ...
	   GLUT_KEY_F1 ... GLUT_KEY_F12
	   GLUT_KEY_UP
	   GLUT_KEY_DOWN
	   GLUT_KEY_LEFT
	   GLUT_KEY_RIGHT
	   GLUT_KEY_PAGE_UP
	   GLUT_KEY_PAGE_DOWN
	   GLUT_KEY_HOME
	   GLUT_KEY_END
	   GLUT_KEY_INSERT
	*/

	switch (key) {

		/* redesenhar o ecra */
		//glutPostRedisplay();
	case GLUT_KEY_F1:
		inicia_modelo();
		glutPostRedisplay();
		break;

	}


	if (DEBUG)
		printf("Carregou na tecla especial %d\n", key);
}

/* Callback para interaccao via teclas especiais (largar na tecla) */
void SpecialKeyUp(int key, int x, int y)
{

	if (DEBUG)
		printf("Largou a tecla especial %d\n", key);

}

/*******************************
***  callbacks do rato       ***
*******************************/

void MouseMotion(int x, int y)
{
	/* x,y    => coordenadas do ponteiro quando se move no rato
				 a carregar em teclas
	*/

	if (DEBUG)
		printf("Mouse Motion %d %d\n", x, y);

}

void MousePassiveMotion(int x, int y)
{
	/* x,y    => coordenadas do ponteiro quando se move no rato
				 sem estar a carregar em teclas
	*/

	if (DEBUG)
		printf("Mouse Passive Motion %d %d\n", x, y);

}

void Mouse(int button, int state, int x, int y)
{
	/* button => GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON
	   state  => GLUT_UP, GLUT_DOWN
	   x,y    => coordenadas do ponteiro quando se carrega numa tecla do rato
	*/

	// alterar o eixo que roda (vari�vel modelo.eixoRodar)

	switch (button) {
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN)
		{
			modelo.eixoRodar = 0;
			estado.movimentoRotacao = !estado.movimentoRotacao;
		}
		break;
	case GLUT_MIDDLE_BUTTON:
		if (state == GLUT_DOWN)
		{
			modelo.eixoRodar = 1;
			estado.movimentoRotacao = !estado.movimentoRotacao;
		}
		break;
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN)
		{
			modelo.eixoRodar = 2;
			estado.movimentoRotacao = !estado.movimentoRotacao;
		}
		break;
	}
	if (DEBUG)
		printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
}
int main(int argc, char **argv)
{
	char str[] = " makefile MAKEFILE Makefile ";

	estado.doubleBuffer = 1;
	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(400, 400);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Exemplo") == GL_FALSE)
		exit(1);

	Init();
	imprime_ajuda();
	/* Registar callbacks do GLUT */

	  /* callbacks de janelas/desenho */
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	/* Callbacks de teclado */
	glutKeyboardFunc(Key);
	//glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	//glutSpecialUpFunc(SpecialKeyUp);

	/* callbacks rato */
	//glutPassiveMotionFunc(MousePassiveMotion);
	//glutMotionFunc(MouseMotion);
	glutMouseFunc(Mouse);

	/* callbacks timer/idle */
	glutTimerFunc(estado.delayMovimento, Timer, 0);
	//glutIdleFunc(Idle);


	/* COMECAR... */
	glutMainLoop();
	return 0;
}